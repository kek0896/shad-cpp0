# Reallol

Это задача типа [crashme](https://gitlab.com/slon/shad-cpp0/blob/master/crash_readme.md).

Исходный код находится в файле `main.cpp`. Исполяемый файл получен командой
```
g++ main.cpp -o /tmp/a.out -std=c++11 -fsanitize=address
```

Советуем не писать ввод каждый раз руками, а сохранить его в файл и
пользоваться перенаправлением ввода
```
./reallol <input
```

Послать ввод из файла на сервер можно командой:
```
(echo reallol; sleep 1; cat input) | nc 35.190.216.192 80
```
